import {dashBoard, download, login,} from "../fixtures/selectors.js";
//import login2 from "../../fixtures/example.json";

describe("Login page Test Scenarios",function(){
    beforeEach(function() {
        cy.visit("/")
     
    });
 it("Should login with valid username and password", function(){
    cy.get(login.email).type('jsanusi07@gmail.com')
    cy.get(login.password).should('be.visible').type('password')
    cy.get(login.loginBtn).should('be.visible').click({ force: true })
    //cy.get(login.location).type('ALAT').wait(2000).type("{downarrow}").type("{enter}").wait(1000)
    cy.get(login.location).type('ALAT')
    cy.get(login.locationOption).click({force:true})
    cy.get(login.continueBtn).click()
    })
    
    it("Should not login with invalid email and password", function(){
        cy.get(login.email).type('tosin@yahoo.com')
        cy.get(login.password).should('be.visible').type('password')
        cy.get(login.loginBtn).should('be.visible').click({ force: true })
        cy.get(login.errorMsg).should('have.text', "Invalid email or password")
    })


    })