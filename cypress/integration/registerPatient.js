import {login, regPatient} from "../fixtures/selectors.js";
//import login2 from "../../fixtures/example.json";

describe("Login page Test Scenarios",function(){
    beforeEach(function() {
        cy.visit("/")
        cy.get(login.email).type('jsanusi07@gmail.com')
        cy.get(login.password).should('be.visible').type('password')
        cy.get(login.loginBtn).should('be.visible').click({ force: true })
        // cy.get(login.location).type('Brancher').wait(2000).type("{downarrow}").type("{enter}").wait(1000)
        cy.get(login.location).type('Brancher')
        cy.get(login.locationOption).click({force:true})
        cy.get(login.continueBtn).click()

    });

    it("should be able to click on frontdesk", function(){
        cy.get(regPatient.frontDeskBtn).click( {force:true })
    })

    it("should be able to click on register a patient", function(){
        cy.get(regPatient.frontDeskBtn).click( {force:true })
        cy.get(regPatient.regPat).click()
    })

    it("should be able to see all personal details fields", function(){
        cy.get(regPatient.frontDeskBtn).click( {force:true })
        cy.get(regPatient.regPat).click( {force:true })
        cy.get(regPatient.Regform).should('have.text', "Registration form")
    })

    it("should be able to fill personal details in fields", function(){
        cy.get(regPatient.frontDeskBtn).click( {force:true })
        cy.get(regPatient.regPat).click( {force:true })
        cy.get(regPatient.Regform).should('have.text', "Registration form")
        cy.get(regPatient.name).type('Ara Ade')
        //cy.get(regPatient.title).select('MISS.').should('have.value','MISS.')
        cy.get(regPatient.title).click()
        cy.get('#react-select-5-option-0').click()
        cy.get(regPatient.gender).click()
        cy.get(regPatient.phoneNum).type("099099890")
        cy.get(regPatient.emailAdd).type('ara@yahoo.com')
        // cy.get(regPatient.addressField).click()
        // cy.get(regPatient.addressField).type('26 Federal Plaza, New York, NY, USA')
        // cy.get(regPatient.location).type('26 Federal Plaza, New York, NY, USA').click()
        // cy.get('#react-select-2-option-0').click()
        // cy.get(regPatient.hmo).click()
        // cy.get(regPatient.hmnProvider).click()
        // cy.get(regPatient.hmofaceook).click({force:true})
        cy.get(regPatient.nOk).click({force:true })
        cy.get(regPatient.nokName).type('emma rose')
        cy.get(regPatient.nokPhone).type('09009009')
        cy.get(regPatient.nokRelation).click()
        cy.get(regPatient.nokRelationBth).click({forec:true})
        cy.get(regPatient.nokEmail).type('emmarose@yahoo.com')
        cy.get(regPatient.selectReferral).type('facebook')
        cy.get(regPatient.allergies).type('One')
        cy.get(regPatient.register).click({force:true})
    })
    // it("should be able to register a patient", function(){
        //  cy.get(regPatient.chooseImage).should()
        //  cy.get(regPatient.plan).type()
        //  cy.get(regPatient.tag).type()
        // })

})