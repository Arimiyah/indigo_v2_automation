import {login,centralStore} from "../fixtures/selectors.js";
//import login2 from "../../fixtures/example.json";

describe("Login page Test Scenarios",function(){
    beforeEach(function() {
        cy.visit("/")
        cy.get(login.email).type('jsanusi07@gmail.com')
        cy.get(login.password).should('be.visible').type('password')
        cy.get(login.loginBtn).should('be.visible').click({ force: true })
        //cy.get(login.location).type('ALAT').wait(2000).type("{downarrow}").type("{enter}").wait(1000)
        cy.get(login.location).type('ALAT')
        cy.get(login.locationOption).click({force:true})
        cy.get(login.continueBtn).click()

    });

        it("Should be able to access the central store", function(){
            
         cy.get(centralStore.centStore).click()
            })

            it("Should be able to switch between tabs", function(){
                cy.get(centralStore.transfer).clicjk()
                cy.get(centralStore.order).click()
                cy.get(centralStore.recall).click()
                //cy.get(login.location).type('ALAT').wait(2000).type("{downarrow}").type("{enter}").wait(1000)
                cy.get(centralStore.return).click()
                
                })

       it("Should be able to view products", function(){
           cy.get(centralStore.productDetails).click('')
          })

     it("Should be able to add and edit product", function(){
           cy.get(centralStore.addProduct).click('')
                        
              })

      it("Should be able to transfer and archive items", function(){
         cy.get(centralStore.email).type('jsanusi07@gmail.com')
                           
          })

     it("Should be able to filter friend", function(){
         cy.get(centralStore.email).type('jsanusi07@gmail.com')
                        
        })
})