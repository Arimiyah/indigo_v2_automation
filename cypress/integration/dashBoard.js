import {dashBoard, login } from "../fixtures/selectors.js";
//import login2 from "../../fixtures/example.json";

describe("Login page Test Scenarios",function(){
    beforeEach(function() {
        cy.visit("/")
        cy.get(login.email).type('jsanusi07@gmail.com')
        cy.get(login.password).should('be.visible').type('password')
        cy.get(login.loginBtn).should('be.visible').click({ force: true })
        cy.get(login.location).type('Brancher')
        cy.get(login.locationOption).click({force:true})
        cy.get(login.continueBtn).click()

    });

    it("should be able to use filter for staff performance", function(){
        cy.get(dashBoard.labouratory).click( {force:true })
        cy.get(dashBoard.dashBtn).click()
        cy.get(dashBoard.filter).click()
        cy.get(dashBoard.filterBranch).type('Brancher')
        cy.get(dashBoard.applyfilter).click()
        cy.get(dashBoard.filterBraBtn).click()
        cy.get(dashBoard.filterStatus).type('completed')
        cy.get(dashBoard.filterStaBtn).click()
        cy.get(dashBoard.filterMonth).type('April')
        cy.get(dashBoard.filterMonBtn).click()
      
    })
    it("should be able to download staff perfomance report", function(){
        cy.get(dashBoard.labouratory).click( {force:true })
        cy.get(dashBoard.dashBtn).click()
        cy.get(dashBoard.downloadBtn).click( {force:true })

    })
    it("should be able to use filter on orders", function(){
        cy.get(dashBoard.labouratory).click( {force:true })
        cy.get(dashBoard.dashBtn).click()
        cy.get(dashBoard.ordersfilter).click( {force:true })
        cy.get(dashBoard.orderbranch).click().type('Brancher')
        cy.get(dashBoard.orderfrom).click( {force:true })
        cy.get(dashBoard.orderaplyfilter).click()
    })

    it("should be able to generate reports on orders", function(){
        cy.get(dashBoard.labouratory).click( {force:true })
        cy.get(dashBoard.dashBtn).click()
        cy.get(dashBoard.ordersdownload).click( {force:true })
        // cy.get(dashBoard.dashBtn).click()
        // cy.get(dashBoard.downloadBtn).click( {force:true })

    })
})